.. _pyFIT3D:

pyFIT3D package
===============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyFIT3D.common
   pyFIT3D.modelling

.. Module contents
.. ---------------
..
.. .. automodule:: pyFIT3D
..    :members:
..    :undoc-members:
..    :show-inheritance:
