.. pyPipe3D documentation master file, created by
   sphinx-quickstart on Tue Oct  5 09:56:14 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyPipe3D's documentation!
====================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   README
   modules
   examples

Introduction
============

This is the documentation of `pyPipe3D <https://gitlab.com/pipe3d/pyPipe3D>`_ (Lacerda et al. in prep), a Python implementation of the `FIT3D + Pipe3D package <http://ifs.astroscu.unam.mx/sfsanchez/FIT3D>`_ (Sanchez et al. 2016 `a <https://ui.adsabs.harvard.edu/abs/2016RMxAA..52...21S/abstract>`_, `b <https://ui.adsabs.harvard.edu/abs/2016RMxAA..52..171S/abstract>`_). **pyPipe3D** is a spectroscopy analysis package which produces coherent, easy to distribute and compare, parameters of the stellar populations and the ionized gas. In addition, this package includes an analysis pipeline (see :ref:`pipeline_readme`) suited in particular for *IFS data cubes* from the most recent optical *IFS* surveys. The pipeline is build using :ref:`pyFIT3D`, which is the main spectral fitting module included in this package.

Main scripts of analysis and plots are located in directory `bin`. The package also contains a set of `examples` (see :ref:`example_fit_elines` and :ref:`example_pipeline`). :ref:`pyFIT3D` is located with the following structure:

.. code-block:: console

 pyFIT3D
 ├── common
 │   ├── __init__.py
 │   ├── auto_ssp_tools.py
 │   ├── constants.py
 │   ├── gas_tools.py
 │   ├── io.py
 │   ├── stats.py
 │   └── tools.py
 ├── __init__.py
 └── modelling
     ├── __init__.py
     ├── dust.py
     ├── gas.py
     └── stellar.py

.. _install:

Installation
------------

If you want to use the modules distributed in this project on your system, you will need a working installation of `Python 3.7` and `pip`. Then you can simply run from the terminal:

.. code-block:: console

 $ pip install pyPipe3D

Otherwise, you can download the source code directly from the `pyPipe3D's GitLab repository <https://gitlab.com/pipe3d/pyPipe3D>`_:

.. code-block:: console

 $ git clone https://gitlab.com/pipe3d/pyPipe3D.git

This will create a directory containing the source code and data sets in `pyPipe3D`, then you install the packages running:

.. code-block:: console

 $ cd pyPipe3D
 $ pyPipe3D> pip install .

As a final step, we recommend that you refresh your terminal shell. After this, you are done.

.. _example_fit_elines:

Test example
------------

**pyPipe3D** includes a `series of scripts <https://gitlab.com/pipe3d/pyPipe3D/-/blob/master/bin>`_ which will be installed at your system after the previous steps. They add a series of algorithms for the fitting of the stellar population and the emission lines of a series of input data types (single spectrum, *data cubes* or *RSS*). The package also includes a series of scripts ready to work with FITS files (e.g. math operations, show header information on screen, create slices from a *data cube*, etc).

We prepare an example of the [NII] + :math:`{\rm H}\alpha` emission lines system spectral fit using Gaussian models. The script chosen is `fit_elines.py <https://gitlab.com/pipe3d/pyPipe3D/-/blob/master/bin/fit_elines.py>`_ and the example is set at sub-directory ``examples``. The call of the script is encapsulated in a shell script. Just run:

.. code-block:: console

 $ pyPipe3D> cd examples
 $ pyPipe3D/examples> source fit_elines_example.sh

Or, call directly the `fit_elines.py <https://gitlab.com/pipe3d/pyPipe3D/-/blob/master/bin/fit_elines.py>`_ script with the right inputs:

.. code-block:: console

 $ pyPipe3D/examples> fit_elines.py NGC5947.cen.gas.txt notebooks/Ha_NII.config none 6600 6800 2 20 5 0 0.15 fit_elines.NII_Ha.NGC5947.out

One of the perks of **pyFIT3D** is the reusability of the code. This helps the users to build their owns analysis python scripts (see `fit_elines_example.py <https://gitlab.com/pipe3d/pyPipe3D/-/blob/master/examples/fit_elines_example.py>`_). An example of a python script to do this example analysis is:

.. code-block:: python
 :linenos:

 import sys
 from pyFIT3D.common.gas_tools import fit_elines

 def main():
     try:
         plot = sys.argv[1]
     except IndexError:
         plot = 0
     w_min, w_max = 6600, 6800
     out_file = 'fit_elines.NII_Ha.NGC5947.out'
     fit_elines(
         'NGC5947.cen.gas.txt', 'notebooks/Ha_NII.config',
         6600, 6800, out_file=out_file, mask_file=None,
         run_mode='BOTH', n_MC=20, n_loops=5, scale_ini=0.15,
         redefine_max=True, plot=plot,
     )

 if __name__ == '__main__':
     main()

.. _example_pipeline:

Example of the pipeline procedure of analysis for a CALIFA data cube
--------------------------------------------------------------------

We prepare an example of the pyPipe3D pipeline analysis of a CALIFA data cube. For the full description of the execution of the pipeline see :ref:`pipeline_readme_example`.

Contact us: `pipe3d@astro.unam.mx <mailto:pipe3d@astro.unam.mx>`_

Indices and tables
==================
* :ref:`modindex`
* :ref:`search`
