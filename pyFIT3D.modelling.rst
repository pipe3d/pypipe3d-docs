pyFIT3D.modelling package
=========================

.. Submodules
.. ----------

pyFIT3D.modelling.dust module
-----------------------------

.. automodule:: pyFIT3D.modelling.dust
   :members:
   :undoc-members:
   :show-inheritance:

pyFIT3D.modelling.gas module
----------------------------

.. automodule:: pyFIT3D.modelling.gas
   :members:
   :undoc-members:
   :show-inheritance:

pyFIT3D.modelling.stellar module
--------------------------------

.. automodule:: pyFIT3D.modelling.stellar
   :members:
   :undoc-members:
   :show-inheritance:

.. Module contents
.. ---------------
..
.. .. automodule:: pyFIT3D.modelling
..    :members:
..    :undoc-members:
..    :show-inheritance:
